package br.com.rodrigoalbinooficial.valemais.valemais;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.AdRequest;

import static br.com.rodrigoalbinooficial.valemais.valemais.Main2Activity.CARTAO_PREFERENCIAS;

public class MainActivity extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;

    private WebView webView;
    private TextView textView3;
    private Button button;
    private WebViewClient webViewClient;
    private Button cadCartao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();//Remove barra de títulos da activity
        setContentView(R.layout.activity_main);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-2931772571979367/6258730329");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2931772571979367/6258730329"); /*id admob*/

        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
            }
        });

        webView = (WebView) findViewById(R.id.webViewId);
        textView3 = (TextView) findViewById(R.id.textView3Id);
        button = (Button) findViewById(R.id.bt1Id);
        cadCartao = (Button) findViewById(R.id.bt_CartaoActivityId);


        webView.setVisibility(View.INVISIBLE);
        webView.loadUrl("https://valemais.marketpay.com.br/LoginModuloCliente?acao=login");
        final WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //Erro de conexão com a Internet
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(final WebView view, int errorCode, String description,final String failingUrl) {
                textView3.setText("SERVIDOR INDISPONIVEL!");
            }
        });

        textView3.setText("(É necessário estar conectado à Internet)");

        cadCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                   //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

                //Restaura as preferencias gravadas
                SharedPreferences settings = getSharedPreferences(CARTAO_PREFERENCIAS, 0);
                final String cartao = settings.getString("prefCartao", "");
                final String senha = settings.getString("prefSenha", "");

                Intent i = new Intent(MainActivity.this, Main2Activity.class);
                    i.putExtra("txtCartao", cartao);
                    i.putExtra("txtSenha", senha);
                startActivity(i);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

                if (textView3.getText() == "(É necessário estar conectado à Internet)") {
                    textView3.setText("Processando...");
                    //webView.getSettings().setJavaScriptEnabled(true);
                    primeiroAcesso();
                    textView3.setTextSize(36);
                    textView3.setTextColor(Color.RED);
                    textView3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }else{
                    textView3.setText("Processando...");
                    webView.loadUrl("javascript:window.location.reload(true)");
                    segundoAcesso();
                    textView3.setTextSize(36);
                    textView3.setTextColor(Color.RED);
                    textView3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
            }
        });
    }

    private void primeiroAcesso() {

        //Restaura as preferencias gravadas
        SharedPreferences settings = getSharedPreferences(CARTAO_PREFERENCIAS, 0);
        final String cartao = settings.getString("prefCartao", "");
        final String senha = settings.getString("prefSenha", "");

        String executaScript1 = "document.getElementById('campoLogin').value = '" + cartao + "';" +
                                "document.getElementById('campoSenha').value = '" + senha + "';" +
                                "document.getElementsByName('button')[0].click();";

        webView.loadUrl("JavaScript:" + executaScript1);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);

                String executaScript2 = "document.getElementById('disponivel').value;";

                webView.evaluateJavascript("JavaScript:" + executaScript2, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String valor) {
                        if (valor.equals("null")){
                            //Toast.makeText(getApplicationContext(), "Texto nulo",Toast.LENGTH_LONG).show();
                            primeiroAcesso();
                        }else {
                            textView3.setText(valor);//Adiciona o valor no TextView
                        }
                    }
                });
            }
        });
    }

    private void segundoAcesso(){

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);

                String executaScript2 = "document.getElementById('disponivel').value;";

                webView.evaluateJavascript("JavaScript:" + executaScript2, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String valor) {
                        textView3.setText(valor);//Adiciona o valor no TextView
                    }
                });
            }
        });
    }
}