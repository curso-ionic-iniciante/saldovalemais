package br.com.rodrigoalbinooficial.valemais.valemais;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    private TextView txtCartao;
    private TextView txtSenha;
    private Button btSalvar;
    public static final String CARTAO_PREFERENCIAS = "Preferências";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();//Remove barra de títulos da activity
        setContentView(R.layout.activity_main2);

        txtCartao = (TextView) findViewById(R.id.txt_CartaoId);
        txtSenha = (TextView) findViewById(R.id.txt_SenhaId);
        btSalvar = (Button) findViewById(R.id.bt_Salvar);

        //Pega a intent que disparou esta Activity
        Intent i = getIntent();

        //Recuperei o texto
        String cartao = i.getStringExtra("txtCartao");
        String senha = i.getStringExtra("txtSenha");

        txtCartao.setText(cartao);
        txtSenha.setText(senha);

        btSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = getSharedPreferences(CARTAO_PREFERENCIAS, MODE_PRIVATE).edit();
                editor.putString("prefCartao", txtCartao.getText().toString());
                editor.putString("prefSenha", txtSenha.getText().toString());
                editor.commit();

        Intent d = new Intent(Main2Activity.this, MainActivity.class);
        startActivity(d);
            }
        });
    }
}
